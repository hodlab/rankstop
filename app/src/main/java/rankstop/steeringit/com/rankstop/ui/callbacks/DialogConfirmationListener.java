package rankstop.steeringit.com.rankstop.ui.callbacks;

public interface DialogConfirmationListener {
    void onCancelClicked();
    void onConfirmClicked(String targetId);
}
