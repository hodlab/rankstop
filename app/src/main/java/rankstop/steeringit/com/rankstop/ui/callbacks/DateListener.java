package rankstop.steeringit.com.rankstop.ui.callbacks;

public interface DateListener {
    void onDateChanged(String date);
}
