package rankstop.steeringit.com.rankstop.utils;

import rankstop.steeringit.com.rankstop.R;

public class RSConstants {


    public static final String PRIVACY_POLICY_LINK = "http://rs.steeringit.lc/PrivacyPolicy";

    public static final String ITEM_CREATED = "created";
    public static final String ITEM_FOLLOWED = "followed";
    public static final String ITEM_OWNED = "owned";
    public static final String TOP_RANKED_ITEMS = "top ranked";
    public static final String TOP_COMMENTED_ITEMS = "top commented";
    public static final String TOP_FOLLOWED_ITEMS = "top followed";
    public static final String TOP_VIEWED_ITEMS = "top viewed";
    public static final String USER_INFO = "user info";
    public static final String FOLLOW_ITEM = "follow item";
    public static final String UNFOLLOW_ITEM = "unfollow item";
    public static final String MY_EVALS = "my evals";
    public static final String ONE_ITEM = "one item";
    public static final String TAB_EVALS = "evals";
    public static final String EMAIL = "email";
    public static final String LOAD_CATEGORIES = "load categories";
    public static final String LOAD_CATEGORY = "load category";
    public static final String ADD_REVIEW = "add review";
    public static final String ADD_ITEM = "add item";
    public static final String RS_ADD_REVIEW = "rs add review";
    public static final String MY_EVAL = "my eval";
    public static final String NAVIGATION_DATA = "navigation data";
    public static final String FROM = "from";
    public static final String LOAD_MY_EVAL = "load my eval";
    public static final String LOGIN = "login";
    public static final String REGISTER = "register";
    public static final String ITEM_COMMENTS = "item comments";
    public static final String ITEM_PIX = "item pix";
    public static final String UPDATE_REVIEW = "update review";
    public static final String LOAD_ABUSES_LIST = "load abuses list";
    public static final String REPORT_ABUSES = "report abuses";
    public static final String ACTION_REPORT_ABUSE = "report abuse";
    public static final String RS_ITEM_DETAILS = "item details";
    public static final String UPDATE_ITEM = "update item";
    public static final String SEARCH = "search";
    public static final String SEARCH_ITEMS = "search items by category and name";
    public static final String ITEM_PIX_BY_USER = "item pics by user";
    public static final String ITEM_COMMENTS_BY_USER = "item comments by user";
    public static final String ITEM = "item";
    public static final String OTHER = "other";
    public static final String MINE = "mine";
    public static final String CURRENT_USER = "user";
    public static final String USER_HISTORY = "user history";
    public static final String USER_ID = "user id";
    public static final String CATEGORY = "category";
    public static final String PICTURE = "picture";
    public static final String PICTURES = "picList";
    public static final String POSITION = "position";
    public static final String COUNT_PAGES = "count_pages";
    public static final String RS_REQUEST_ITEM_DATA = "rs_request_item_data";
    public static final String FILTER = "filter";
    public static final String FILTERED_PICTURES = "filtered pictures";
    public static final String MY_PIX = "my pix";
    public static final String ALL_PIX = "all pix";
    public static final String COUNTRIES_LIST = "countries list";
    public static final String CURRENT_DATE = "currentDate";
    public static final String SOCIAL_LOGIN = "social login";
    public static final String PROVIDER_GOOGLE = "GOOGLE";
    public static final String PROVIDER_FB = "FACEBOOK";
    public static final String UPDATE_PROFILE = "update profile";
    public static final String ITEM_ID = "item id";
    public static final String ITEM_NAME = "item name";
    public static final String SEND_REQ_OWNER_SHIP = "send request ownership";
    public static final String ACTION_SEND_REQ_OWNERSHIP = "action send request ownership";
    public static final double FAKE_LATITUDE = 36.7948624;
    public static final double FAKE_LONGITUDE = 10.073238;
    public static final String COMMENT = "comment";
    public static final String MESSAGE = "message";
    public static final String DELETE_COMMENT = "delete comment";
    public static final String DELETE_PICTURE = "delete picture";
    public static final String RS_CONTACT = "rs contact";
    public static final String PUBLIC_IP = "public ip";
    public static final String ACTION_EVAL = "action eval";
    public static final String ACTION_COMMENT = "action comment";
    public static final String ACTION_PIX = "action pix";
    public static final String RS_ACTION = "rs action";
    public static final String LIST_NOTIFS = "notifications list";
    public static final String EDIT_NOTIF_VISIBILITY = "edit notif visibility";
    public static final int MIN_AGE_USER = 10;
    public static final String FORMAT_DATE = "date format";
    public static final String FORGOT_PWD = "forgot password";
    public static final String LOAD_CATEGORIES_USED_BY_LOCATION = "load categories used by location";
    public static final String SEARCH_ITEMS_FILTERED = "search items filtered";
    public static String _ID = "id";

    // parameters
    public static final int MAX_ITEM_TO_LOAD = 5;
    public static final int MAX_GALLERY_PIX = 5;
    public static final int REQUEST_CODE = 1;
    public static final int MAX_FIELD_TO_LOAD = 5;

    //fake user
    public static final String HEADER_TOKEN = "X-Access-Token";
    public static final String PASSWORD_FAKE_USER = "rankstop-android";
    public static final String EMAIL_FAKE_USER = "rankStop-android@steeringit.com";
    public static final int CODE_TOKEN_EXPIRED = 401;
    public static final int CODE_NO_TOKEN = 403;

    // colors
    public static final int PIE_GREEN = R.color.colorGreenPie;
    public static final int PIE_ORANGE = R.color.colorOrangePie;
    public static final int PIE_RED = R.color.colorRedPie;

    // actions
    public static final String ACTION_FOLLOW = "action follow";
    public static final String ACTION_ADD_REVIEW = "action add review";
    public static final String ACTION_CONNECT = "action connect";

    // fragments
    public static final String FRAGMENT_ADD_ITEM = "add item fragment";
    public static final String FRAGMENT_SETTINGS = "settings fragment";
    public static final String FRAGMENT_HISTORY = "history fragment";
    public static final String FRAGMENT_NOTIF = "notif fragment";
    public static final String FRAGMENT_LISTING_ITEMS = "listing items fragment";
    public static final String FRAGMENT_SEARCH = "search fragment";
    public static final String FRAGMENT_UPDATE_ITEM = "update item";
    public static final String FRAGMENT_ADD_REVIEW = "add review fragment";
    public static final String FRAGMENT_PROFILE = "profile fragment";
    public static final String FRAGMENT_MY_EVALS = "my evals fragment";
    public static final String FRAGMENT_SIGN_UP = "signup fragment";
    public static final String FRAGMENT_HOME = "home fragment";
    public static final String FRAGMENT_ITEM_DETAILS = "item details fragment";
    public static final String FRAGMENT_UPDATE_PROFILE = "fragment update profile";
}
