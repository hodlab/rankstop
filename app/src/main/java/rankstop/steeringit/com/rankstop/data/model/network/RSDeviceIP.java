package rankstop.steeringit.com.rankstop.data.model.network;

import java.io.Serializable;

public class RSDeviceIP implements Serializable {

    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
