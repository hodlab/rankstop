package rankstop.steeringit.com.rankstop.data.model.network;

import java.io.Serializable;

public class ResponseAddItem implements Serializable {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
