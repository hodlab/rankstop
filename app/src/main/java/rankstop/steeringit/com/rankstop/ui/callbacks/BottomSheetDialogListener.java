package rankstop.steeringit.com.rankstop.ui.callbacks;

public interface BottomSheetDialogListener {
    void onTakePictureClicked();
    void onChoosePictureClicked();
}
