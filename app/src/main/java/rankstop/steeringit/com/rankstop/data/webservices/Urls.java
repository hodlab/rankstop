package rankstop.steeringit.com.rankstop.data.webservices;

public class Urls {
    public static final String MAIN_URL = "http://rs-ws.steeringit.lc:2500/api/";
    //public static final String MAIN_URL = "http://192.168.7.113:2500/api/";

    public static final String GEO_PLUGIN_URL = "http://www.geoplugin.net/";
    public static final String IP_FINDER = "https://api.ipify.org";
}
