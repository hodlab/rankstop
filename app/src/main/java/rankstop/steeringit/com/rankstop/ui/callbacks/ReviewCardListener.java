package rankstop.steeringit.com.rankstop.ui.callbacks;

import android.view.View;

public interface ReviewCardListener {
    void onRemoveClicked(int position);
    void onClick(View view, int position);
}
